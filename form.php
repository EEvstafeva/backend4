<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css"> 
  <title>Задание 4</title>
</head>
<body>
  <div class="main">
    <?php 
    if ($errorOutput) {
      print('<section id="messages">');
      if ($hasErrors)
        print('<h2>Ошибка</h2>');
      else
        print('<h2>Сообщения</h2>');
      print($errorOutput);
      print('</section>');
    }
    ?>
    <section id="form">
      <h1>Форма</h1>
      <form action="."
        method="POST">
        <label>
          Имя<br />
          <input name="name" <?php if (!empty($errors['name'])) {print 'class="error"';} ?>
            value="<?php print $values['name']; ?>"/>
        </label><br />

        <label>
          E-mail:<br />
          <input name="email" <?php if (!empty($errors['email'])) {print 'class="error"';} ?>
            value="<?php print $values['email']; ?>"
            type="email" />
        </label><br />

        <label>
          Дата рождения:<br />
          <input name="birthday" <?php if (!empty($errors['birthday'])) {print 'class="error"';} ?>
            value="<?php print $values['birthday']; ?>"
            type="date" />
        </label><br />

        Пол:<br />
        <label><input type="radio"
          name="gender" value="M" <?php if ($values['gender'] == 'M') {print 'checked';} ?>/>
          мужской
        </label>
        <label><input type="radio"
          name="gender" value="F" <?php if ($values['gender'] == 'F') {print 'checked';} ?> />
          женский
        </label>
        <br />

        Количество конечностей:<br />
        <label><input type="radio" <?php if ($values['limbs'] == '0') {print 'checked';} ?>
          name="limbs" value="0" />
          0
        </label>
        <label><input type="radio" <?php if ($values['limbs'] == '1') {print 'checked';} ?>
          name="limbs" value="1" />
          1
        </label>
        <label><input type="radio" <?php if ($values['limbs'] == '2') {print 'checked';} ?>
          name="limbs" value="2" />
          2
        </label>
        <label><input type="radio" <?php if ($values['limbs'] == '3') {print 'checked';} ?>
          name="limbs" value="3" />
          3
        </label>
        <br />

        <label>
          Сверхспособности:
          <br />
          <select name="superpowers[]"
            multiple="multiple">
            <option value="1" <?php if ($values['superpowers']['1']) {print 'selected';} ?>>Бессмертие</option>
            <option value="2" <?php if ($values['superpowers']['2']) {print 'selected';} ?>>Невидимость</option>
            <option value="3" <?php if ($values['superpowers']['3']) {print 'selected';} ?>>Левитация</option>
            <option value="4" <?php if ($values['superpowers']['4']) {print 'selected';} ?>>Огнеупорность</option>
          </select>
        </label><br />

        <label>
          Биография:<br />
          <textarea name="biography"><?php print $values['biography']; ?></textarea>
        </label><br />

        <br />
        <label <?php if (array_key_exists('contract', $errors)) {print 'class="error"';} ?>>
        <input type="checkbox"
          name="contract" <?php if ($values['contract']) {print 'checked';} ?>/>
          С контрактом ознакомлен
        </label><br />
        
        <input id="submit" type="submit" value="Отправить" />
      </form>
    </section>
  </div>
</body>
</html>
