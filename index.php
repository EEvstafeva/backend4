<?php
header('Content-Type: text/html; charset=UTF-8');
$errorOutput = '';
$errors = array();
$hasErrors = FALSE;

$defaultValues = [
  'name' => '',
  'email' => '',
  'birthday' => '',
  'gender' => 'M',
  'limbs' => '2',
  'biography' => 'Что-то о себе...',
  'contract' => ''
];

$values = array();
foreach (['name', 'email', 'birthday', 'gender', 'limbs', 'biography', 'contract'] as $key) {
  $values[$key] = !array_key_exists($key . '_value', $_COOKIE) ? $defaultValues[$key] : $_COOKIE[$key . '_value'];
}
foreach (['name', 'email', 'birthday'] as $key) {
  $errors[$key] = empty($_COOKIE[$key . '_error']) ? '' : $_COOKIE[$key . '_error'];
  if ($errors[$key] != '')
    $hasErrors = TRUE;
}

$values['superpowers'] = array();
$values['superpowers']['1'] = empty($_COOKIE['superpowers_1_value']) ? '' : $_COOKIE['superpowers_1_value'];
$values['superpowers']['2'] = empty($_COOKIE['superpowers_2_value']) ? '' : $_COOKIE['superpowers_2_value'];
$values['superpowers']['3'] = empty($_COOKIE['superpowers_3_value']) ? '' : $_COOKIE['superpowers_3_value'];
$values['superpowers']['4'] = empty($_COOKIE['superpowers_4_value']) ? '' : $_COOKIE['superpowers_4_value'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    $errorOutput = 'Результаты сохранены.<br/>';
  }

  if (!empty($errors['name'])) {
    $errorOutput .= 'Введите имя.<br/>';
  }
  if (!empty($errors['email'])) {
    $errorOutput .= 'Введите email.<br/>';
  }
  if (!empty($errors['birthday'])) {
    $errorOutput .= 'Введите дату рождения.<br/>';
  }

  include('form.php');
  exit();
}

$trimmedPost = [];
foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

if (empty($trimmedPost['name'])) {
  $errorOutput .= 'Заполните имя.<br/>';
  $errors['name'] = TRUE;
  setcookie('name_error', 'true');
  $hasErrors = TRUE;
} else {
  setcookie('name_error', '', 10000);
  $errors['name'] = '';
}
setcookie('name_value', $trimmedPost['name'], time() + 12 * 30 * 24 * 60 * 60);
$values['name'] = $trimmedPost['name'];

if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  $errorOutput .= 'Заполните email.<br/>';
  $errors['email'] = TRUE;
  setcookie('email_error', 'true');
  $hasErrors = TRUE;
} else {
  setcookie('email_error', '', 10000);
  $errors['email'] = '';
}
setcookie('email_value', $trimmedPost['email'], time() + 12 * 30 * 24 * 60 * 60);
$values['email'] = $trimmedPost['email'];

if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  $errorOutput .= 'Введите дату рождения.<br/>';
  $errors['birthday'] = TRUE;
  setcookie('birthday_error', 'true');
  $hasErrors = TRUE;
} else {
  setcookie('birthday_error', '', 10000);
  $errors['birthday'] = '';
}
setcookie('birthday_value', $trimmedPost['birthday'], time() + 12 * 30 * 24 * 60 * 60);
$values['birthday'] = $trimmedPost['birthday'];

if (!preg_match('/^[MF]$/', $trimmedPost['gender'])) {
  $errorOutput .= 'Введите пол.<br/>';
  $errors['gender'] = TRUE;
  $hasErrors = TRUE;
}
setcookie('gender_value', $trimmedPost['gender'], time() + 12 * 30 * 24 * 60 * 60);
$values['gender'] = $trimmedPost['gender'];

if (!preg_match('/^[0-3]$/', $trimmedPost['limbs'])) {
  $errorOutput .= 'Заполните количество конечностей.<br/>';
  $errors['limbs'] = TRUE;
  $hasErrors = TRUE;
}
setcookie('limbs_value', $trimmedPost['limbs'], time() + 12 * 30 * 24 * 60 * 60);
$values['limbs'] = $trimmedPost['limbs'];

foreach (['1', '2', '3', '4'] as $value) {
  setcookie('superpowers_' . $value . '_value', '', 10000);
  $values['superpowers'][$value] = FALSE;
}
if (array_key_exists('superpowers', $trimmedPost)) {
  foreach ($trimmedPost['superpowers'] as $value) {
    if (!preg_match('/[0-3]/', $value)) {
      $errorOutput .= 'Что-то не то!<br/>';
      $errors['superpowers'] = TRUE;
      $hasErrors = TRUE;
    }
    setcookie('superpowers_' . $value . '_value', 'true', time() + 12 * 30 * 24 * 60 * 60);
    $values['superpowers'][$value] = TRUE;
  }
}
setcookie('biography_value', $trimmedPost['biography'], time() + 12 * 30 * 24 * 60 * 60);
$values['biography'] = $trimmedPost['biography'];
if (!isset($trimmedPost['contract'])) {
  $errorOutput .= 'Вы не ознакомились с контрактом.<br/>';
  $errors['contract'] = TRUE;
  $hasErrors = TRUE;
}

if ($hasErrors) {
  include('form.php');
  exit();
}

$user = 'u41080';
$pass = '2491072';
$db = new PDO('mysql:host=localhost;dbname=u41080', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
   $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO forma SET name = ?, email = ?, birthday = ?, 
    gender = ? , limbs = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limbs'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO clients_capabilities SET id_client = ?, id_ability = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['abilitiess'] as $s){
    $stmt2 -> execute([$id, $s]);

$stmt3 = $db->prepare("INSERT INTO capabilities SET id_ability = ?, name_ability = ?");
$stmt3 -> execute([$s, form.hero.abilitiess[$s].value]);  
  }
  $db->commit();

}
catch(PDOException $e){
  $db->rollBack();
  $errorOutput = 'Error : ' . $e->getMessage();
  include('form.php');
  exit();
}

header('Location: ?save=1');
